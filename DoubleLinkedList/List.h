#ifndef LIST_H
#define LIST_H

#include <stdlib.h>
#include <assert.h>

template<typename L> class List
{
private:
    struct Node
    {
        L data;

        Node* prev;
        Node* next;

        Node(L d) : data (d), prev (0), next (0) {}

        Node(L d, Node* _prev, Node* _next) : data (d), prev (_prev), next (_next) {}
    };
    Node* _beg;
    Node* _end;
    int size;
    void swap (List& other)
    {
        std::swap(_beg,other._beg);
        std::swap(_end,other._end);
    }

public:
    struct iterator
    {
        Node* ptr;

        iterator (Node* _ptr = 0) : ptr (_ptr) {}

        L& operator* ()
        {
            return ptr -> data;
        }

        iterator& operator++ ()
        {
            ptr = ptr -> next;
            return *this;
        }

        iterator& operator++ (int)
        {
            iterator res(this -> ptr);
            ptr = ptr -> next;
            return res;
        }

        iterator& operator-- ()
        {
            if (ptr)
            {
                ptr = ptr -> prev;
                return *this;
            }
        }

        iterator& operator-- (int)
        {
            if (ptr)
            {
                iterator res(this -> ptr);
                ptr = ptr -> prev;
                return res;
            }
        }

        iterator& operator= (const iterator& res)
        {
            this -> ptr = res.ptr;
            return *this;
        }

        bool operator== (const iterator& other) const
        {
            return (ptr == other.ptr);
        }

        bool operator!= (const iterator& other) const
        {
            return (ptr != other.ptr);
        }
    };

    List ()
    {
        size=0;
        _beg=new Node(NULL,NULL,_end);
        _end=new Node(NULL,_beg,NULL);
    }

    List (List const& src_list)
    {
        size=0;
        _beg=new Node(NULL,NULL,_end);
        _end=new Node(NULL,_beg,NULL);
        Node* node = src_list._beg->next;
        while(node != src_list._end)
        {
            push_back(node -> data);
            node=node -> next;
        }
    }

    List& operator= (List src_list)
    {
        this -> clear();
        this -> swap(src_list);
        return *this;
    }

    ~List ()
    {
        clear();
    }

    int Size()
    {
        return size;
    }

    bool isEmpty()
    {
        return (size==0);
    }

    void push_back (L const& dt)
    {
        Node* temp = new Node(dt);
        if (size==0)
        {
            temp->prev=_beg;
            temp->next=_end;
            _beg->next=temp;
            _end->prev=temp;
            size++;
        }
        else
        {
            Node* last=_end->prev;
            last->next=temp;
            temp->prev=last;
            temp->next=_end;
            _end->prev=temp;
            size++;
        }
    }

    void push_front (L const& dt)
    {
        Node* temp = new Node(dt);
        if(size==0)
        {
            temp->prev=_beg;
            temp->next=_end;
            _beg->next=temp;
            _end->prev=temp;
            size++;
        }
        else
        {
            Node* front=_beg->next;
            front->prev=temp;
            temp->next=front;
            temp->prev=_beg;
            _beg -> next = temp;
            size++;
        }
    }

    void pop_back ()
    {
        if (size != 0)
        {
            Node* last = _end -> prev;
            _end -> prev = last -> prev;
            (last -> prev) -> next = _end;
            delete last;
            size--;
        }
    }

    void pop_front ()
    {
        if (size != 0)
        {
            Node* temp = _beg -> next;
            _beg -> next = temp -> next;
            (temp -> next) -> prev = _beg;
            delete temp;
            size--;
        }
    }

    L& back ()
    {
        if (size != 0)
        {
            return _end -> prev -> data;
        }
    }

    L const& back() const
    {
        if (size != 0)
        {
            return _end -> prev -> data;
        }
    }

    L& front()
    {
        if (size != 0)
        {
            return _beg -> next -> data;
        }
    }

    L const& front() const
    {
        if (size != 0)
        {
            return _beg -> next -> data;
        }
    }

    iterator begin ()
    {
        return _beg -> next;
    }

    iterator end ()
    {
        return _end;
    }

    void erase (iterator key)
    {
        Node* temp = key.ptr;
        (temp -> prev) -> next = temp -> next;
        (temp -> next) -> prev = temp -> prev;
        delete temp;
    }

    void clear ()
    {
        if (_beg != 0)
        {
            Node* pv = _beg;
            while(pv)
            {
                pv = pv -> next;
                delete _beg;
                _beg = pv;
            }
        }
        _beg = _end = 0;
    }

    iterator insert (iterator key, L dt)
    {
        Node* pkey=key.ptr;
        Node* temp=new Node(dt);
        if(size==0)
        {
            temp->prev=_beg;
            temp->next=_end;
            _beg->next=temp;
            _end->prev=temp;
            size++;
        }
        else
        {
            (pkey->prev)->next=temp;
            temp->prev=pkey->prev;
            temp->next=pkey;
            pkey->prev=temp;
            size++;
        }
        return temp;
    }
};
#endif // LIST_H
