#define BOOST_TEST_MODULE list_test
#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MAIN
#include <boost/test/unit_test.hpp>
#include "List.h"

BOOST_AUTO_TEST_CASE(push_pop_back)
{
    List<int> l;
    for(int i=0;i<=20;++i)
    {
        l.push_back(i);
    }
    BOOST_CHECK_EQUAL(l.back(),20);
    l.pop_back();
    BOOST_CHECK_EQUAL(l.back(),19);
}

BOOST_AUTO_TEST_CASE(push_pop_front)
{
    List<int> l;
    for(int i=0;i<=20;++i)
    {
        l.push_front(i);
    }
    BOOST_CHECK_EQUAL(l.front(),20);
    l.pop_front();
    BOOST_CHECK_EQUAL(l.front(),19);
}

BOOST_AUTO_TEST_CASE(erase)
{
    List<int> l;
    for(int i=0;i<=20;++i)
    {
        l.push_back(i);
    }
    l.erase(l.begin());
    l.erase(--l.end());
    BOOST_CHECK_EQUAL(l.front(),1);
    BOOST_CHECK_EQUAL(l.back(),19);
}

BOOST_AUTO_TEST_CASE(insert)
{
    List<int> l;
    for(int i=0;i<=20;++i)
    {
        l.push_back(i);
    }
    l.insert(l.begin(),100);
    l.insert(l.end(),100);
    BOOST_CHECK_EQUAL(l.front(),100);
    BOOST_CHECK_EQUAL(l.back(),100);
}

BOOST_AUTO_TEST_CASE(operat)
{
    List<int> l;
    for(int i=1;i<=10;++i)
    {
        l.push_back(i);
    }
    List<int> oper;
    for(int i=10;i<=15;++i)
    {
        oper.push_back(i);
    }
    oper=l;
    List<int>::iterator i=oper.begin();
    BOOST_CHECK_EQUAL(*i,1);
    BOOST_CHECK_EQUAL(*boost::next(i),2);
    i=oper.end();
    BOOST_CHECK_EQUAL(*boost::prior(i),10);
    --i;
    BOOST_CHECK_EQUAL(*boost::prior(i),9);
}

BOOST_AUTO_TEST_CASE(copy)
{
    List<int> l;
    for(int i=1;i<=2;++i)
    {
        l.push_back(i);
    }
    List<int> copy(l);
    BOOST_CHECK_EQUAL(l.front(),copy.front());
    BOOST_CHECK_EQUAL(l.back(),copy.back());
}

BOOST_AUTO_TEST_CASE(iterator_dec_inc_oper)
{
    List<int> l;
    for(int i=1;i<=3;++i)
    {
        l.push_back(i);
    }
    List<int>::iterator i=l.begin();
    List<int>::iterator i2=--l.end();
    BOOST_CHECK_EQUAL(*boost::next(i),*boost::prior(i2));
    bool t1=(i!=i2);
    bool t2=(i==i2);
    BOOST_CHECK_EQUAL(t1,true);
    BOOST_CHECK_EQUAL(t2,false);
}

BOOST_AUTO_TEST_CASE(test_mail)
{
    List<int> l;
    l.insert(l.begin(),10);
    BOOST_CHECK_EQUAL(l.front(),10);
    List<int> k;
    k.insert(k.end(),10);
    BOOST_CHECK_EQUAL(k.front(),10);
}
