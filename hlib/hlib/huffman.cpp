#include "huffman.h"

namespace Huffman
{
	//���� ���������� ���� ����� ��� � ���� ������: http://habrahabr.ru/post/144200/
	//��� ���������, �� ����� ���������, ����� ���� ������ � ����� ������� �
	//������� ��� ���� ����.

	//�������� ��� ���������� ������ FList � ������ CreateTree
	bool CompareNodes(Node* a, Node* b)
	{
		return a->freq < b->freq;
	}

	//����������� ������ ������ ��� �������� � ������
	PackManager::PackManager(): EncodedData(NULL), DecodedData(NULL)
	{
	}

	//���������� ����������� ������. 
	PackManager::~PackManager()
	{
		if(EncodedData) free(EncodedData); //free - ������ ��� ��� ������ realloc
		if(DecodedData) free(DecodedData);
	}
	
	//�������� ������� �� ������
	void PackManager::CreateTable(Node* nd)
	{
		if(nd->left != NULL)			//������� �����: �������� �� ������ ����������,
		{								//��������� ������ ���. 0 - ���� ������� �����
			vCode.push_back(0);			//1 - ���� ������� ������.
			CreateTable(nd->left);
		}
		if(nd->right != NULL)
		{
			vCode.push_back(1);
			CreateTable(nd->right);
		}
		if(!nd->left && !nd->right)		//���� ��� ������� �������� � ���� ������
		{
			table[nd->byte] = vCode;	//�� ���������� ���������� ��� ������� ����������
		}
		vCode.pop_back();
	}

	//�������� ������ �� ������ (� ������ ���� ������� � ������������, �� ����� ������� ��� ��)
	Node* PackManager::CreateTree(list<Node*> &FList)
	{
		while(FList.size() > 1)			//���� � ������ �� ��������� ���� ������� (��������)
		{
			FList.sort(CompareNodes);	//��������� ��� �� ����������� ������� ��������

			Node* left  = FList.front(); FList.pop_front();	 //����� ������ ��� �������� �� ������
			Node* right = FList.front(); FList.pop_front();

			Node* parent = new Node(0, left->freq + right->freq); //��������� �� ��� ����� ����, � �������� �������
				  parent->left  = left;							  //����� ����� ������ ��� ��������
				  parent->right = right;

			FList.push_back(parent);	//���������� ���� ���� ������� � ������
		}

		return FList.front();	//��� ������ ������� ���� ������� - ��� ��������, ������� ��� � ����������
	}

	//������������ ������ �� ��� ������
	void DestroyTree(Node* p)	
	{
		if(p)							//������ ���� �������� ����������
		{								//� ������� ����
			DestroyTree(p->left);
			DestroyTree(p->right);
			delete p;
		}
	}

	//�������� ������
	HFHEADER PackManager::Pack(uchar *inBytes, long inSize)
	{	
		if(!inBytes || inSize < 1)
		{
			throw exception("Pack Error: Invalid input bytes!");
		}
		
		long	Freq[256];						//������� ������ ������ � �������� ��� ������
		memset(Freq, 0, sizeof(Freq)); 
		vCode.clear();

		for(long i = 0; i < inSize; ++i) ++Freq[inBytes[i]];	//������������ ������� ��������� ������� ����� � inBytes

		list<Node*> FList;		
		for(int i = 0; i < 256; ++i) 
		{
			if(Freq[i] > 0)	FList.push_back(new Node(i, Freq[i]));	//��������� ������ ��� ������, � ������� ������� �� 0
		}

		Node* root = CreateTree(FList);			//������� ������ � ������ ������� ��� �����������
		CreateTable(root);
		
		long   bsize = inSize;		//������ ����������� ������ ������, � ������� ����� ��������� �������������� ������
		uchar* buf   = (uchar*) calloc(bsize, sizeof(uchar)); //���� �����������, �� ��� ����� ��������.

		char count = 0;	 //�.�. ������������������ ����� �������� ��������, �� ��� ����� �������������
		char byte  = 0;	 //������� ��� � byte ��� ���� ��������, ����� ������� ���������� byte � �����
		int  pos   = 0;	 //������� ������� � ������

		for(long i = 0; i < inSize; ++i)	//���������� �� ���� ������������������
		{
			vCode = table[inBytes[i]];		//������� ������� ����������� ��� ��� �������� �����

			for(unsigned int j = 0; j < vCode.size(); ++j)	//������ �������� ��������� ���� ��� � �����
			{
				byte |= vCode[j]<<(7 - count);
				
				if(++count == 8)			//���� � byte ��� ���� �������� 8 ���, ��
				{							
					buf[pos++] = byte;		//���������� byte � �����
					if(pos == bsize)		//���� �� �������� ������� ������
					{
						bsize *= 2;			//�� �������� ��� � 2 ����
						buf = (uchar*) realloc(buf, bsize); 
					}
					byte = 0; count = 0;	//������� byte, ��� ��� ���������� ����������
				}
			}
		}
		if(count != 0){ buf[pos++] = byte; } //���� ��������� ������������ ����, �� ��� �������� �� �����, �� ����������
											 //���� ������� � ����� ������
		
		DestroyTree(root);	//������� ������ �����

		EncodedSize = pos;							//���������� ������ �������������� ������
		EncodedData = (uchar*) realloc(buf, pos);	//���������� ���� ������

		HFHEADER result = {							//��������� ����� ��� ������
							{'H','F','P','K'},		//�� ������ ������ 'HFPK'
							{0},						//���������� ���� ������
							pos								//����� ������ ������
						  };
		memcpy(result.Freq, Freq, sizeof(Freq));    //�������� ������� ������ � �����

		return result;
	}

	
	//����������
	void PackManager::UnPack(uchar *inBytes, long inSize, HFHEADER& head)
	{
		list<Node*> FList;

		for(int i = 0; i < 256; ++i)	//��������� ����� ��� ������ � ��������� ��������
		{
			if(head.Freq[i] > 0) FList.push_back(new Node(i, head.Freq[i]));
		}

		Node* root = CreateTree(FList);	//������� ������ ��� �������������

		Node* temp = root;				//������� ������� � ������

		long   bsize   = root->freq;	//�� ������� ����� ������� ����� �������� ���� �� ������
										//�.�. � ������ �������� ��� ����, � ���� ������� ��������� ��������
		uchar* buf     = (uchar*) calloc(bsize, sizeof(uchar));  //������� ������� ����� ������� �������

		long  pos = 0;	//������� ������� � ������

		uchar byte;
		for(long i = 0; i < inSize; ++i)
		{
			byte = inBytes[i];			//������ ���� ������
			for(int j = 7; j >= 0; --j)	//� �������� �������� ������������� ���
			{
				if((byte >> j) & 1)		//���� ��� ����� �������
				{
					temp = temp->right;	//��������� �� ������ ������
				}
				else					//� ��������� ������ �����
				{
					temp = temp->left;
				}
				if((!temp->left) && (!temp->right)) //���� �� �������� � ���� ������, �� ������ �� ������
				{
					buf[pos++] = temp->byte;	//������ ���� �� ����� ������ � ����� ��� � �����
					if(pos == bsize) goto End;	//���� �� ������� ��, ��� ������������, �� ���������
												//������������ ��� � ����� goto, ��� ���� ����� ������

					temp = root;				//��, � ���� �� �� �������, �� ������������ � �����					
				}
			}
		}

		//����� ��� goto (�� ������ ����, ��� ������)
		End:
			DestroyTree(root);	//����� � ��� �� ����
			DecodedSize = pos;
			DecodedData = buf;
	}

	//���-���-���
	long	PackManager::GetEncodedSize() { return EncodedSize; }
	long	PackManager::GetDecodedSize() { return DecodedSize; }

	uchar*	PackManager::GetEncodedData() const { return EncodedData; }
	uchar*	PackManager::GetDecodedData() const { return DecodedData; }
}