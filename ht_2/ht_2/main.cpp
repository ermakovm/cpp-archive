#include <fstream>
#include <iostream>
#include <string>
#include <sstream>
#include <cstdio>
#include <cstdlib>
#include <vector>
using namespace std;


bool checkSort(vector<int> &a)
{
	for (int k = 0; k < (int) a.size() - 1; k++)
	{
		if (a[k] > a[k + 1])
			return false;
	}
	return true;
}

void quickSort1(vector<int> &a, int l, int r, long long &comps)
{
	if (l >= r)
		return;
	int idxp = l, i = l, temp;
	for (int j = l + 1; j <= r; j++)
	{
		if (a[j] < a[idxp])
		{
			++i;
			temp = a[j];
			a[j] = a[i];
			a[i] = temp;
		}
	}
	temp = a[l];
	a[l] = a[i];
	a[i] = temp;
	comps += r - l;
	quickSort1(a, l, i - 1, comps);
	quickSort1(a, i + 1, r, comps);
}

void quickSort2(vector<int> &a, int l, int r, long long &comps)
{
	if (l >= r)
		return;
	int temp = a[r];
	a[r] = a[l];
	a[l] = temp;
	int idxp = l, i = l;
	for (int j = l + 1; j <= r; j++)
	{
		if (a[j] < a[idxp]) 
		{
			++i;
			temp = a[j];
			a[j] = a[i];
			a[i] = temp;
		}
	}
	temp = a[l];
	a[l] = a[i];
	a[i] = temp;
	comps += r - l;
	quickSort2(a, l, i - 1, comps);
	quickSort2(a, i + 1, r, comps);
}


void quickSort3(vector<int> &a, int l, int r, long long &comps)
{
	if (l >= r)
		return;
	int pivot = (l + r) / 2;
	if ((a[l] < a[pivot] && a[l] > a[r]) || (a[l] > a[pivot] && a[l] < a[r]))
		pivot = l;
	if ((a[r] < a[pivot] && a[r] > a[l]) || (a[r] > a[pivot] && a[r] < a[l]))
		pivot = r;
	int temp = a[pivot];
	a[pivot] = a[l];
	a[l] = temp;
	int idxp = l, i = l;
	for (int j = l + 1; j <= r; j++)
	{
		if (a[j] < a[idxp])
		{
			++i;
			temp = a[j];
			a[j] = a[i];
			a[i] = temp;
		}
	}
	temp = a[l];
	a[l] = a[i];
	a[i] = temp;
	comps = comps + r - l;
	quickSort3(a, l, i - 1, comps);
	quickSort3(a, i + 1, r, comps);
}


int main() {
	string InputFileName = "QuickSort.txt";
	ifstream infile(InputFileName.c_str());
	if (!infile.is_open())
	{
		cerr << "Error with file opening: "<<InputFileName<<endl;
		exit(1);
	}
	vector<int> IntArray, NewArray1, NewArray2, NewArray3;
	string templine;
	while (!infile.eof())
	{
		getline(infile, templine);
		if (templine.size() == 0)
			continue;
		IntArray.push_back(std::atoi(templine.c_str()));
	}
	infile.close();
	cout << IntArray.size() << " numbers acquired." << endl;
	NewArray1 = IntArray;
	NewArray2 = IntArray;
	NewArray3 = IntArray;
	long long comps1 = 0, comps2 = 0, comps3 = 0;
	quickSort1(NewArray1, 0, NewArray1.size() - 1, comps1);
	quickSort2(NewArray2, 0, NewArray2.size() - 1, comps2);
	quickSort3(NewArray3, 0, NewArray3.size() - 1, comps3);
	if (!checkSort(NewArray1)) {
		cerr << "Wrong with this quickSort1\n";
	}
	if (!checkSort(NewArray2)) {
		cerr << "Wrong with this quickSort2\n";
	}
	if (!checkSort(NewArray3)) {
		cerr << "Wrong with this quickSort3\n";
	}
	cout << "# of comparisons in Problem 1:\t" << comps1 << endl;
	cout << "# of comparisons in Problem 2:\t" << comps2 << endl;
	cout << "# of comparisons in Problem 3:\t" << comps3 << endl;
	return 0;
}