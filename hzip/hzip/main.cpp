#include <iostream>
#include <string>
#include <cstring>
#include <fstream>
#include <climits>
#include <clocale>

#include "huffman.h"
#pragma comment(lib, "hlib")

#define MAX_FILES 10

using namespace std; 

typedef unsigned char uchar;

//������� ��� �������� ����� � ������ fnameIN
void   Pack(string fnameIN)
{
	Huffman::PackManager hpack;		//������� ��������� (��. ��������� ����������)
	Huffman::HFHEADER	 hhead;		//������� ����� � ������� ����� ���� ��� ��������������.

	ifstream fin(fnameIN,	ios::binary);	//�������� ������ � ����������� �������� �� ������������
	if(!fin)
	{
		cout << ">> Cannot open file: " << fnameIN << endl;
		return;
	}

	ofstream fout(fnameIN + ".htk", ios::binary);
	if(!fout)
	{
		cout << ">> Cannot create output file for: " << fnameIN << endl;
		fin.close();
		return;
	}

	uchar*	fbytes;		//��� ����� � �����
	long	fsize;		//��� ������ �����

	fin.seekg(0, ios::end);				//������� ������ �����
	fsize = (long) fin.tellg();

	if(fsize > (LONG_MAX/4))			//��������� �� ������. ����� ������ 512 �� �������������� �� �����.
	{
		cout << ">> Too large file: " << fnameIN << endl;
		fin.close(); fout.close();
		return;
	}

	fbytes = new uchar[fsize + 1];		//������� ������ ����

	fin.seekg(0, ios::beg);				//������ ��� ����� �� �����
	fin.read((char*) fbytes, fsize);

	cout << ">> Pack " << fnameIN <<  " ---> ";
	hhead = hpack.Pack(fbytes, fsize);	//������ � ��������� ������ ���� � ���������� ����. �� ������ ����� ���� ��� ��������������

	fout.write((char*) &hhead, sizeof(hhead));							//����� � ������ ����� ��������� � ������� ��� ��������������
	fout.write((char*) hpack.GetEncodedData(), hpack.GetEncodedSize());	//����� �������������� �����

	cout << fnameIN << ".htk" << endl;

	delete[] fbytes;
	
	fin.close();
	fout.close();
}

void UnPack(string fnameIN)
{
	Huffman::PackManager hpack;		//������� �����������	
	Huffman::HFHEADER	 hhead;		//� ��������� � ����� ��� ��������������

	int	inSZ = fnameIN.size();		//�������� �� ���������� �����
	if(inSZ > 4)
	{
		string extention = fnameIN.substr(inSZ - 4, string::npos);
		if(extention != ".htk")
		{
			cout << ">> File " << fnameIN << " is not huffman packed!" << endl;
			return;
		}
	}

	string fnameOUT = fnameIN.substr(0, inSZ - 4); //������� ���������� .htk �� ����� ��������� �����

	ifstream fin(fnameIN,	ios::binary);	//����� ����������� ��������
	if(!fin)
	{
		cout << ">> Cannot open file: " << fnameIN << endl;
		return;
	}

	ofstream fout(fnameOUT, ios::binary);
	if(!fout)
	{
		cout << ">> Cannot create output file for: " << fnameIN << endl;
		fin.close();
		return;
	}

	uchar*	fbytes;							

	fin.read((char*) &hhead, sizeof(hhead)); //��������� ��������� �� �����
	
	fbytes = new uchar[hhead.DataSize];		 //������� ������ ����
	
	fin.read((char*) fbytes, hhead.DataSize); //������ ����� �� �����

	cout << ">> Unpack " << fnameIN <<  " ---> ";
	hpack.UnPack(fbytes, hhead.DataSize, hhead); //������ ��� ����� �� ����������.
	
	fout.write((char*) hpack.GetDecodedData(), hpack.GetDecodedSize()); //����� ������� � ����

	cout << fnameOUT << endl;

	fin.close();
	fout.close();
}

int main(int argc, char *argv[])
{
	if(argc < 2)
	{
		cerr << endl << ">> Incorrect arguments list!" << endl << endl;
		exit(-1);
	}
		
	string	fnameIN [MAX_FILES];
	string	buf;

	int fcount = 0;
	for(int i = 1; i < argc; ++i)
	{
		buf = argv[i];

		if(buf == "-pack")  
		{
			for(int j = 0; j < fcount; ++j)
				Pack(fnameIN[j]);

			fcount = 0; 
			continue;
		}

		if(buf == "-unpack")
		{ 
			for(int j = 0; j < fcount; ++j)
				UnPack(fnameIN[j]);

			fcount = 0;
			continue;
		}

		fnameIN[fcount++] = buf;
		
		if(fcount == 10) 
		{
			break;
		}
	}

	if(fcount > 0)
	{
		for(int i = 0; i < fcount; ++i)
			Pack(fnameIN[i]);
	}

	system("PAUSE");
	return 0;
}