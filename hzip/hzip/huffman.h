#pragma once

#include <exception>
#include <cstdlib>
#include <vector>
#include <list>

using namespace std;

typedef unsigned char uchar;

namespace Huffman
{
	//��������� ��� ������������ ���� ������
	struct Node
	{
		long	freq;	//������� ���������� byte �� ���� ������ ������
		uchar	byte;	//����, ������� ������ ����
		Node*	left;	
		Node*	right;

		_declspec(dllexport) Node(uchar b, long f) : byte(b), freq(f), left(NULL), right(NULL) {}
	};

	//����� ��� ������ ��������� �����������
	//����� ������ ��� � ������ �����
	struct HFHEADER
	{
		char	B[4];		//���� ����� ������ 'HFPK' ��� ������������� ������
		long	Freq[256];	//������� � ��������� ����
		long	DataSize;	//������ ������������ ������
	};

	//�����, � ������� ����������� �����������
	class PackManager
	{
	private:
		vector<bool> table[256];	//������� ��� ����������� ������� �����, ������� �� �������� �� ������
		vector<bool> vCode;			//�������� ������� ��� �������� � ������ �� �������

		uchar*	EncodedData;		//������ ����� ������� ������ ����� ��������
		uchar*	DecodedData;		//� ������ ����� ����������
		
		long	EncodedSize;		//������ ������������ ������
		long	DecodedSize;		//������ ������������� ������

	public:
		_declspec(dllexport) PackManager();				//�����������/����������/���-���-���
		_declspec(dllexport) ~PackManager();

		_declspec(dllexport) HFHEADER	Pack	(uchar *inBytes, long inSize);					//���������� ��������������� ����, ������ � inSize
		_declspec(dllexport) void		UnPack  (uchar *inBytes, long inSize, HFHEADER& head);	//������������� ������������������ ����. ���� ���
																			//�������� ������ ����� �� ��������� head
		_declspec(dllexport) long	GetEncodedSize();			//���-���-���
		_declspec(dllexport) long	GetDecodedSize();

		_declspec(dllexport) uchar*	GetEncodedData() const;
		_declspec(dllexport) uchar*	GetDecodedData() const;

	private:
		_declspec(dllexport) void	CreateTable(Node* nd);				//������� ������� ��� ����������� �� ������
		_declspec(dllexport) Node*	CreateTree(list<Node*> &FList);		//������� ������ �� ������
	};
}