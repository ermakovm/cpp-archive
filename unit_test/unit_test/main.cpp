#include <iostream>
#include <ctime>
#include <cstdlib>

#include "huffman.h"
#pragma comment(lib, "hlib")

typedef unsigned char uchar;

using namespace std;

//��������� ���������� ����� � ��������� �� 10000 �� 20000 
//�� ��� �������� ��� ����, ����� ���������� ��������������
//����������� ��� �������� �� �������
long GetRandSize()
{
	 time_t timer;
	 struct tm y2k;
	 double seconds;
	
	 y2k.tm_hour = 0;   y2k.tm_min = 0; y2k.tm_sec = 0;
 	 y2k.tm_year = 100; y2k.tm_mon = 0; y2k.tm_mday = 1;

	 time(&timer);  
		 
	 seconds = difftime(timer,mktime(&y2k));

	 int ent = (long)seconds % 3;

	 if(ent == 0)
	 { 
		 seconds = (long long)seconds*(rand() % 4 + 1) % 10999; srand(seconds); 
	 }
	 else if(ent == 1)
	 { 
		 seconds = (long long)seconds % 1000; srand(seconds);
	 }
	 else if(ent == 2)
	 { 
		 seconds = (long long )seconds % 5699; srand(seconds); 
	 }

	return (rand()*(rand() % 3 + 1) % 10000) + 10000;
}

void InitBlockRandValues(uchar *block, long nBlockSize)
{
	for(int i = 0; i < nBlockSize; i++)
	{
		block[i] = 65 + rand() % 27;
	}
}


int main()
{
	long	nRandSize = GetRandSize();
	uchar*	rawData	  = new uchar[nRandSize + 1];

	InitBlockRandValues(rawData, nRandSize); 
	cout << "Data size before packing: " << nRandSize << endl;

	Huffman::PackManager	hfPack;
	Huffman::HFHEADER		hfHead;

	hfHead = hfPack.Pack(rawData, nRandSize);
		
	cout << "Data size after packing: " << hfPack.GetEncodedSize() << endl;

	hfPack.UnPack(hfPack.GetEncodedData(), hfPack.GetEncodedSize(), hfHead);
	cout << "Data size after unpacking: " << hfPack.GetDecodedSize() << endl;

	if(!memcmp(hfPack.GetDecodedData(), rawData, nRandSize))
		cout<<"Data before encode and after is equal!\n";

	system("pause");
	return 0;
}